package com.blueraptor.p2o.receiver;

import com.blueraptor.p2o.receiver.service.SendService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class P2oTxApplication {
    public static SendService sendService;

    public static void main(String[] args) throws IOException, TimeoutException {

        SpringApplication.run(P2oTxApplication.class, args);
    }

}
