package com.blueraptor.p2o.receiver.service;

import com.blueraptor.p2o.receiver.domain.Message;
import com.blueraptor.p2o.receiver.repo.MessageRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@Service("senderService")
public class SendService {
    static Logger logger = LoggerFactory.getLogger(SendService.class);

    private static final String QUEUE_NAME = "demo-q";

    private MessageRepository messageRepository;
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    @Autowired
    public SendService(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
        this.factory = new ConnectionFactory();
        this.factory.setUsername("guest");
        this.factory.setPassword("guest");
        this.factory.setHost("192.168.2.46");
        this.factory.setPort(5672);

        try {
            this.connection = this.factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        try {
            this.channel = this.connection.createChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Message createMessage(String message, String source, String targetQ) {
        return messageRepository.save(new Message(message, source, targetQ));
    }

    public void fire(Message message) throws IOException, TimeoutException {

        String msgText = message.getMessage() + ": " + message.getId();
        channel.basicPublish("", message.getTargetQ(), null,msgText.getBytes(StandardCharsets.UTF_8));
        logger.info("[v2 SendService] Sent '" + msgText + "'");

        // The Consumer connects to the same Message Repository as the Publisher updated records.
        List<Message> allMessages = (List<Message>) this.messageRepository.findAll();

        logger.info("[v2 SendService] Repo Dump: ");
        for (Message msg : allMessages) {
            logger.info(msg.toString());
        }
    }

    public Optional<Message> fetchMessage(Integer Id) {
        Optional<Message> msg = messageRepository.findById(Id);
        logger.info("[v2 SendService] fetchMessage '" + msg.toString() + "'");
        return msg;
    }

    public Message updateMessage(Message message) {
        return messageRepository.save(message);
    }
}
