package com.blueraptor.p2o.receiver.controller;

import com.blueraptor.p2o.receiver.domain.Message;
import com.blueraptor.p2o.receiver.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@RestController
public class PublishController {
    SendService senderService;

    @Autowired
    public PublishController(SendService sendService) {
        this.senderService = sendService;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public void sendMessage() throws IOException, TimeoutException {
        Message msg = this.senderService.createMessage("Repository Message","New Sender", "demo-q");
        this.senderService.fire(msg);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateMessage() throws IOException, TimeoutException {
        Message msg = null;
        this.senderService.updateMessage(msg);
    }

    @RequestMapping(path = "/fetch/msgId/{id}", method = RequestMethod.GET)
    public Optional<Message> fetchMessage(@PathVariable int id) throws IOException, TimeoutException {
        Optional<Message> msg = this.senderService.fetchMessage(id);
        msg.toString();
        return msg;
    }
}
