package com.blueraptor.p2o.receiver.repo;

import com.blueraptor.p2o.receiver.domain.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface MessageRepository extends PagingAndSortingRepository<Message,Integer> {
    @Override
    Iterable<Message> findAll(Sort sort);

    @Override
    Page<Message> findAll(Pageable pageable);

    @Override
    <S extends Message> S save(S s);

    @Override
    <S extends Message> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Optional<Message> findById(Integer integer);

    @Override
    boolean existsById(Integer integer);

    @Override
    Iterable<Message> findAll();

    @Override
    Iterable<Message> findAllById(Iterable<Integer> iterable);

    @Override
    long count();

    @Override
    void deleteById(Integer integer);

    @Override
    void delete(Message message);

    @Override
    void deleteAll(Iterable<? extends Message> iterable);

    @Override
    void deleteAll();
}
