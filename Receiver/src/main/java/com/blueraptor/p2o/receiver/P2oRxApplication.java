package com.blueraptor.p2o.receiver;

import com.blueraptor.p2o.receiver.service.ReceiveService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class P2oRxApplication {
    public static ReceiveService receiveService;

    public static void main(String[] args) throws IOException, TimeoutException {

        SpringApplication.run(P2oRxApplication.class, args);
    }

}
