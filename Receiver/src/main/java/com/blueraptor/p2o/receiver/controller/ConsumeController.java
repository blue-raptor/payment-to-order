package com.blueraptor.p2o.receiver.controller;

import com.blueraptor.p2o.receiver.service.ReceiveService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


// Receive service call

@RestController
public class ConsumeController {
    ReceiveService receiverService;
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    @Autowired
    public ConsumeController(ReceiveService receiverService) {
        this.receiverService = receiverService;
    }

    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    public void receiveMessage() throws Exception {
        this.receiverService.consume();
    }

    @RequestMapping(path = "/receive/msgId/{id}", method = RequestMethod.GET)
    public void receiveMessagebyId(@PathVariable int id) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://localhost:8080/sender_war_exploded/fetch/msgId/" + id))
                .setHeader("User_Agent", "consumer bot")
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // print status code
        System.out.println("receiveMessagebyId Status Code: " + response.statusCode());

        // print response body
        System.out.println("receiveMessagebyId Body: " + response.body());
    }
}
