package com.blueraptor.p2o.receiver.service;

import com.blueraptor.p2o.receiver.domain.Message;
import com.blueraptor.p2o.receiver.repo.MessageRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Service("receiverService")
public class ReceiveService {

    private MessageRepository messageRepository;
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    private final static String QUEUE_NAME = "demo-q";
    static Logger logger = LoggerFactory.getLogger(ReceiveService.class);

    @Autowired
    public ReceiveService(MessageRepository messageRepository){

        this.messageRepository = messageRepository;
        this.factory = new ConnectionFactory();
        this.factory.setUsername("guest");
        this.factory.setPassword("guest");
        this.factory.setHost("192.168.2.46");
        this.factory.setPort(5672);

        try {
            this.connection = this.factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        try {
            this.channel = this.connection.createChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void consume() throws Exception {

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            logger.info("[v2] ReceiveService: Received '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });

    }
}