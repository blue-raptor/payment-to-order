package com.blueraptor.p2o.sender_repo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SenderRepoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SenderRepoApplication.class, args);
	}

}
