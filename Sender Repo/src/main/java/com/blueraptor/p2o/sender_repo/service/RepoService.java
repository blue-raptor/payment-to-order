package com.blueraptor.p2o.sender_repo.service;

import com.blueraptor.p2o.sender_repo.domain.Message;
import com.blueraptor.p2o.sender_repo.repo.MessageRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("senderService")
public class RepoService {
    static Logger logger = LoggerFactory.getLogger(RepoService.class);

    private MessageRepository messageRepository;

    @Autowired
    public RepoService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message createMessage(Message msg) {
        return messageRepository.save(new Message(msg.getMessage(),
                msg.getSource(),
                msg.getTargetQ()));
    }

    public Message createMessage(String message, String source, String targetQ) {
        return messageRepository.save(new Message(message, source, targetQ));
    }

}
