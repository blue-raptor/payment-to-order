package com.blueraptor.p2o.sender_repo.controller;

import com.blueraptor.p2o.sender_repo.domain.Message;
import com.blueraptor.p2o.sender_repo.service.RepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

@RestController
public class RepoController {
    RepoService repoService;

    @Autowired
    public RepoController(RepoService repoService) {

        this.repoService = repoService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createMessage() throws IOException, TimeoutException {
        Message msg = this.repoService.createMessage("Repository Message","New Sender", "demo-q");
    }

}
